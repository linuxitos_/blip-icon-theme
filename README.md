### Qué es?

Blip theme es un tema de íconos para gnome shell, puede funcionar para otro, sin embargo fue modificado para funcionar enteramente con gnome, por si hubiera algún problema con algún otro entorno de escritorio, es por ese motivo.

- Los principales cambios que le aplicaron a éste fork
	- Iconos para carpetas
	- Mimes types para archivos como latex, ini, config, php, apache config, bib
	- Actualización de nombres de archivos para apps de flatpak
	- íconos para configuración de nautilus, configaración de gnome-text-editor
	- Iconos para apps com zoom, libreoffice, debeaver, jasperstudio, pycharm, sublimetext
	- Y muchos más...

![alt tag](back/1.png)

![alt tag](back/2.png)

![alt tag](back/3.png)

```
cd ~/.icons
git clone https://gitlab.com/linuxitos_/blip-icon-theme.git
```


### Configuration
Blip theme es un tema que se puede descargar y colocar en la carpeta:

- crear una carpeta de nombre .icons en el directorio personal
- Descomprimir los archivos dentro de ese directorio
- Instalar gnome-tweak-tool para establecer el tema de iconos
- Dentro de la carpeta de iconos existen varios iconos personalizados para carpetas como jasper, netbeans, android sdk, pycharm.


### License
Blip Theme is fully free software that is dual-licensed under the MIT
and GPLv3 licenses.

Much of the artwork in La Capitaine is based on Numix Circle and El
General GNOME (now known as Antu), with significant additions and
changes. This art is to be treated as a GPLv3 licensed library.

The included configuration script is licensed under the permissive MIT
license.

See the `LICENSE` and `COPYING` files for more details.